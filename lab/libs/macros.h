

#ifndef _MACROS_H 
    
    #define create_threads(__newthread, __attr, __start_routine, __arg, nthreads) {                      \
                                                                        \
        for(int i=0; i<nthreads; i++) {                                 \
            if(pthread_create(__newthread+i, __attr, __start_routine, (void*) (__arg+i))){               \
                puts("ERRO--pthread_create"); exit(3);                  \
            }                                                           \
        }                                                               \
    }



    #define join_threads(__th, __thread_return, nthreads) {             \
                                                                        \
        for(int i=0; i<nthreads; i++) {                                 \
            pthread_join(*(__th+i), __thread_return);                   \
        }                                                               \
    }
  
#endif

/*
for(int i=0; i<nthreads; i++) {
      pthread_join(*(tid+i), NULL);
   }
   */