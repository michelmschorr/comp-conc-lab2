#ifndef _INCLUDE_H
    #include <stdio.h>
    #include <stdlib.h>
    #include <pthread.h>
    #include <time.h>

    #include "../libs/timer.h"

    #include "functions.h"
    
    #include "macros.h"
#endif