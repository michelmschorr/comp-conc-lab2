#include <stdio.h>
#include <stdlib.h>

#include "structs.h"



//cria as args
tArgs* taskManager(int dim, int nthreads){

  tArgs* args; //identificadores locais das threads e elementos
  
  

  const int matrixSize = (dim*dim);
  const int excessElements = matrixSize%nthreads;
  const int elementsByThread = (matrixSize-excessElements)/nthreads;




  //alocando os agrs
  args = (tArgs *) malloc(sizeof(tArgs)*nthreads);
  if (args == NULL) {printf("ERRO--malloc\n"); exit(4);}



  //inicializando os agrs
  for(int i =0; i<nthreads-1; i++){
    args[i].dim = dim;
    args[i].elmntsNumber = elementsByThread;


    //alocando espaco para as posicoes dos elementos desse arg
    args[i].positions = (int *)malloc(sizeof(int)*elementsByThread);
    if (args[i].positions == NULL) {printf("ERRO--malloc\n"); exit(5);}

    //inicializando as posicoes
    for(int j =0;     j<args[i].elmntsNumber;     j++){
      args[i].positions[j] = i*elementsByThread+j;
    }
  }



  //inicializando o ultimo agr
  args[nthreads-1].dim = dim;
  args[nthreads-1].elmntsNumber = elementsByThread+excessElements;

  args[nthreads-1].positions = (int *)malloc(sizeof(int)*(elementsByThread+excessElements));





  for(int j =0;     j<args[nthreads-1].elmntsNumber;     j++){
      args[nthreads-1].positions[j] = (nthreads-1)*elementsByThread+j;
  }



  return args;
}
