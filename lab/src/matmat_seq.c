/* Multiplicacao de matriz-vetor (considerando matrizes quadradas) */
#include<stdio.h>
#include<stdlib.h>
#include "../libs/timer.h"
#include<time.h>

float *matE1; //matriz de entrada
float *matE2; //vetor de entrada
float *matSaida; //vetor de saida


//fluxo principal
int main(int argc, char* argv[]) {
   int dim; //dimensao da matriz de entrada
   
 
   double inicio, fim, delta;
   
   GET_TIME(inicio);
   //leitura e avaliacao dos parametros de entrada
   if(argc<2) {
      printf("Digite: %s <dimensao da matriz>\n", argv[0]);
      return 1;
   }
   dim = atoi(argv[1]);
    //printf("%d\n", dim);






   //alocacao de memoria para as estruturas de dados
   matE1 = (float *) malloc(sizeof(float) * dim * dim);
   if (matE1 == NULL) {printf("ERRO--malloc\n"); return 2;}
   matE2 = (float *) malloc(sizeof(float) * dim * dim);
   if (matE2 == NULL) {printf("ERRO--malloc\n"); return 2;}
   matSaida = (float *) malloc(sizeof(float) * dim * dim);
   if (matSaida == NULL) {printf("ERRO--malloc\n"); return 2;}







   //inicializacao das estruturas de dados de entrada e saida
   for(int i=0; i<dim; i++) {
      for(int j=0; j<dim; j++){
          //printf("====%d=====\n", i*dim+j);
         matE1[i*dim+j] = 1;
         matE2[i*dim+j] = 1; 
         matSaida[i*dim+j] = 0;     //equivalente mat[i][j]
        }
   }
   GET_TIME(fim);
   delta = fim - inicio;
   //printf("Tempo inicializacao:%lf\n", delta);






   //multiplicacao da matriz pelo vetor
   GET_TIME(inicio);
   
    for(int i=0; i<dim; i++)
      for(int j=0; j<dim; j++) 
        for(int k=0; k<dim; k++)
         matSaida[i*(dim) + j] += matE1[i*(dim) + k] * matE2[k*(dim) + j];

   GET_TIME(fim)   
   delta = fim - inicio;
   printf("Tempo multiplicacao (dimensao %d): %lf\n", dim, delta);






  //printando os resultados para teste
  /*
  printf("Matriz 1\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matE1[i*dim+j]);
      }
      printf("\n");
    }
    printf("\n");

    printf("Matriz 2\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matE2[i*dim+j]);
      }
      printf("\n");
    }
    printf("\n");

    printf("Matriz Saida\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matSaida[i*dim+j]);
      }
      printf("\n");
    }
  */





   //liberacao da memoria
   GET_TIME(inicio);
   free(matE1);
   free(matE2);
   free(matSaida);

   GET_TIME(fim)   
   delta = fim - inicio;
   //printf("Tempo finalizacao:%lf\n", delta);






    



   return 0;
}
