/* Multiplicacao de matriz-matriz (considerando matrizes quadradas) */

#include "../libs/include.h"


float *matE1; //matriz de entrada
float *matE2; //vetor de entrada
float *matSaida; //vetor de saida
int nthreads; //numero de threads





//Vai calcular o valor de todos os elementos da matriz de saida requeridos
void * task(void *arg) {
  //printf("Entrando task\n");
  tArgs args = *((tArgs*) arg);

  int lin, col;
  for(int i=0; i<args.elmntsNumber; i++){

    col = args.positions[i] % args.dim;
    lin = (args.positions[i] - col)/ args.dim;


    //calcula o valor do elemento[lin][col] na matriz de saida
    for(int k=0; k<args.dim; k++){
      matSaida[lin*args.dim + col]     += 
        matE1[lin*args.dim + k]   *
        matE2[k*args.dim + col];
      }
  }
    //printf("Saindo task\n");
   pthread_exit(NULL);
}









//fluxo principal
int main(int argc, char* argv[]) {

  int dim; //dimensao da matriz de entrada
  pthread_t *tid; //identificadores das threads no sistema

  tArgs* args; //argumentos das threads

  double inicio, fim, delta; //usados pra medir tempo
   






  GET_TIME(inicio);


  //leitura e avaliacao dos parametros de entrada
  if(argc<3) {
    printf("Digite: %s <dimensao da matriz> <numero de threads>\n", argv[0]);
    return 1;
  }

  dim = atoi(argv[1]);
  nthreads = atoi(argv[2]);

  if (nthreads > dim*dim) nthreads=dim*dim;
  //printf("%d\n", dim);


  



  //alocacao de memoria para as estruturas de dados
  matE1 = (float *) malloc(sizeof(float) * dim * dim);
  if (matE1 == NULL) {printf("ERRO--malloc\n"); return 2;}
  matE2 = (float *) malloc(sizeof(float) * dim * dim);
  if (matE2 == NULL) {printf("ERRO--malloc\n"); return 2;}
  matSaida = (float *) malloc(sizeof(float) * dim * dim);
  if (matSaida == NULL) {printf("ERRO--malloc\n"); return 2;}


  



   //inicializacao das estruturas de dados de entrada e saida
  for(int i=0; i<dim; i++) {
    for(int j=0; j<dim; j++){
      //printf("====%d=====\n", i*dim+j);
      matE1[i*dim+j] = 1;
      matE2[i*dim+j] = 1; 
      matSaida[i*dim+j] = 0;     //equivalente mat[i][j]
    }
  }




  GET_TIME(fim);

  delta = fim - inicio;
  printf("Tempo inicializacao:%lf\n", delta);







  //multiplicacao da matriz pelo vetor

  GET_TIME(inicio);


  tid = (pthread_t*) malloc(sizeof(pthread_t)*nthreads);
  if(tid==NULL) {puts("ERRO--malloc"); return 2;}



  //cria as args
  args = taskManager(dim, nthreads);


  //cria as threads
  create_threads(tid, NULL, task, args, nthreads);
   

  //espera pelo termino da threads
  join_threads(tid, NULL, nthreads);




  GET_TIME(fim)   
  delta = fim - inicio;
  printf("Tempo multiplicacao (dimensao %d | threads %d): %lf\n", dim, nthreads, delta);






  //printando os resultados para teste
  /*
  printf("Matriz 1\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matE1[i*dim+j]);
      }
      printf("\n");
    }
    printf("\n");

    printf("Matriz 2\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matE2[i*dim+j]);
      }
      printf("\n");
    }    
    printf("\n");

    printf("Matriz Saida\n");
    for(int i=0; i<dim; i++){
      for(int j=0; j<dim; j++) {
        printf("%f ", matSaida[i*dim+j]);
      }
      printf("\n");
    }
    */





   //liberacao da memoria
   GET_TIME(inicio);
   free(matE1);
   free(matE2);
   free(matSaida);

   GET_TIME(fim)   
   delta = fim - inicio;
   printf("Tempo finalizacao:%lf\n", delta);



   return 0;
}
