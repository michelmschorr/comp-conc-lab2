# Computação Concorrente

## Laboratório 2 - Multiplicação matriz matriz concorrente

<br>

### Michel Monteiro Schorr 120017379

<br><br>

### Dentro da pasta lab, irá encontrar 3 pastas:  
**execs**, contendo os executaveis  
**libs**, contendo as bibliotecas, macros, funcoes, estruturas, etc  
**src**, contendo o condigo fonte principal

<br><br>

## Comandos

Os comandos para compilar e executar são os seguintes:
<br>
<br>

### funcao de multiplicação matriz matriz concorrente

``` 
gcc ./lab/src/matmat_conc.c -o ./lab/execs/matmat_conc -Wall -lpthread
./lab/execs/matmat_conc dim threads
```

<br>

### funcao de multiplicação matriz matriz sequencial

```
gcc ./lab/src/matmat_seq.c -o ./lab/execs/matmat_seq -Wall
./lab/execs/matmat_seq dim
```

<br>

### funcao de multiplicação matriz vetor concorrente

```
gcc ./lab/src/matvet.c -o ./lab/execs/matvet -Wall -lpthread
./lab/execs/matvet dim threads
```

<br>
